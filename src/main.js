import { createApp } from 'vue'
import App from '@/App.vue'
import '@/index.css'
import AppButton from '@/components/AppButton.vue'
import router from '@/router'
import VueFinalModal from 'vue-final-modal'

const app = createApp(App)

app.component('AppButton', AppButton)
app.use(VueFinalModal())
app.use(router)
app.mount('#app')
